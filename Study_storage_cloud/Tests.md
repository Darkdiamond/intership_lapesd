# Tests

## What will we test ?

We're going to try and compare the computational performance of an amazon spot instance with EBS, an amazon spot instance with FSx Lustre and a cluster on grid5000.

## Why will we test ?

We want to know whether or not using FSx Lustre can be cost-effective, given that it is more expensive. In particular, we want to know whether its higher cost is offset by its computational efficiency.

## How will we test ?

We plan to use different benchmarks focused on memory accesses, in order to evaluate read-write throughput. To do this we will use a checkpoint system that will write its checkpoints to memory. Note that the aim is not to read the memory cache.

## The plan :

### previous one :

|Benchmarks | instances | Processor | Storage solution | Number of parallel processes | Checkpoints | Time to execute | throughput (if available) | cost |
|---  |---  |---  |:-:  |:-:  |:-:  |:-:  |:-:  |--: |
|HEAT  | instance spot |c5n.large  |EBS  |1  |ULFM  |?  |?  |? |
|HEAT  | instance spot |c5n.large  |EBS  |4  |ULFM |?  |?  |? |
|HEAT  | instance spot |c5n.large  |FSx Lustre  |1  |ULFM  |?  |?  |? |
|HEAT  | instance spot |c5n.large  |FSx Lustre  |4  |ULFM  |?  |?  |? |
|---  |---  |---  |---  |---  |---  |---  |---  |---  |
|HEAT  | instance spot |c5n.large  |EBS  |1  |BLCR  |?  |?  |? |
|HEAT  | instance spot |c5n.large  |EBS  |4  |BLCR |?  |?  |? |
|HEAT  | instance spot |c5n.large  |FSx Lustre  |1  |BLCR  |?  |?  |? |
|HEAT  | instance spot |c5n.large  |FSx Lustre  |4  |BLCR  |?  |?  |? |
|---  |---  |---  |---  |---  |---  |---  |---  |---  |
|NPB-IS  | instance spot |c5n.large  |EBS  |1  |ULFM  |?  |?  |? |
|NPB-IS  | instance spot |c5n.large  |EBS  |4  |ULFM |?  |?  |? |
|NPB-IS  | instance spot |c5n.large  |FSx Lustre  |1  |ULFM  |?  |?  |? |
|NPB-IS  | instance spot |c5n.large  |FSx Lustre  |4  |ULFM  |?  |?  |? |
|---  |---  |---  |---  |---  |---  |---  |---  |---  |
|NPB-IS  | instance spot |c5n.large  |EBS  |1  |BLCR  |?  |?  |? |
|NPB-IS  | instance spot |c5n.large  |EBS  |4  |BLCR |?  |?  |? |
|NPB-IS  | instance spot |c5n.large  |FSx Lustre  |1  |BLCR  |?  |?  |? |
|NPB-IS  | instance spot |c5n.large  |FSx Lustre  |4  |BLCR  |?  |?  |? |
|---  |---  |---  |---  |---  |---  |---  |---  |---  |
|Dynemol | instance spot |c5n.large  |EBS  |1  |ULFM  |?  |?  |? |
|Dynemol | instance spot |c5n.large  |EBS  |4  |ULFM |?  |?  |? |
|Dynemol | instance spot |c5n.large  |FSx Lustre  |1  |ULFM  |?  |?  |? |
|Dynemol | instance spot |c5n.large  |FSx Lustre  |4  |ULFM  |?  |?  |? |
|---  |---  |---  |---  |---  |---  |---  |---  |---  |
|Dynemol  | instance spot |c5n.large  |EBS  |1  |BLCR  |?  |?  |? |
|Dynemol  | instance spot |c5n.large  |EBS  |4  |BLCR |?  |?  |? |
|Dynemol  | instance spot |c5n.large  |FSx Lustre  |1  |BLCR  |?  |?  |? |
|Dynemol  | instance spot |c5n.large  |FSx Lustre  |4  |BLCR  |?  |?  |? |

### Actual one :

|Benchmarks | instances type | Processor | Number of nodes | Storage solution | Number of parallel processes | Checkpoints systeme | matrix's size | Time to execute | throughput (if available) | cost |
|---  |---  |---  |:-:  |:-:  |:-:  |:-:  |:-:  |:-:  |:-:  |--: |
|HEAT  | instance spot |c5n.xlarge  | 1 |EBS  |4   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 1 |EBS  |4   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 1 |EBS  |4   | 2048 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 4 |EBS  |4   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 4 |EBS  |4   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 4 |EBS  |4   | 2048 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 4 |EBS  |16   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 4 |EBS  |16   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 4 |EBS  |16   | 2048 |SCR |?  |?  |? |
|---  |---  |---  |---  |---  |---  |---  |---  |---  |---  |--- |
|HEAT  | instance spot |t3.xlarge  | 1 |EBS  |4   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 1 |EBS  |4   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 1 |EBS  |4   | 2048 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 4 |EBS  |4   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 4 |EBS  |4   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 4 |EBS  |4   | 2048 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 4 |EBS  |16   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 4 |EBS  |16   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 4 |EBS  |16   | 2048 |SCR |?  |?  |? |
|---  |---  |---  |---  |---  |---  |---  |---  |---  |---  |--- |
|HEAT  | instance spot |c6g.xlarge  | 1 |EBS  |4   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 1 |EBS  |4   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 1 |EBS  |4   | 2048 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 4 |EBS  |4   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 4 |EBS  |4   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 4 |EBS  |4   | 2048 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 4 |EBS  |16   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 4 |EBS  |16   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 4 |EBS  |16   | 2048 |SCR |?  |?  |? |
|===  |===  |===  |===  |===  |===  |===  |===  |===  |===  |=== |
|HEAT  | instance spot |c5n.xlarge  | 1 |FSx Lustre  |4   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 1 |FSx Lustre  |4   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 1 |FSx Lustre  |4   | 2048 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 4 |FSx Lustre  |4   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 4 |FSx Lustre  |4   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 4 |FSx Lustre  |4   | 2048 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 4 |FSx Lustre  |16   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 4 |FSx Lustre  |16   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |c5n.xlarge  | 4 |FSx Lustre  |16   | 2048 |SCR |?  |?  |? |
|---  |---  |---  |---  |---  |---  |---  |---  |---  |---  |--- |
|HEAT  | instance spot |t3.xlarge  | 1 |FSx Lustre  |4   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 1 |FSx Lustre  |4   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 1 |FSx Lustre  |4   | 2048 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 4 |FSx Lustre  |4   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 4 |FSx Lustre  |4   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 4 |FSx Lustre  |4   | 2048 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 4 |FSx Lustre  |16   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 4 |FSx Lustre  |16   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |t3.xlarge  | 4 |FSx Lustre  |16   | 2048 |SCR |?  |?  |? |
|---  |---  |---  |---  |---  |---  |---  |---  |---  |---  |--- |
|HEAT  | instance spot |c6g.xlarge  | 1 |FSx Lustre  |4   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 1 |FSx Lustre  |4   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 1 |FSx Lustre  |4   | 2048 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 4 |FSx Lustre  |4   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 4 |FSx Lustre  |4   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 4 |FSx Lustre  |4   | 2048 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 4 |FSx Lustre  |16   | 256 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 4 |FSx Lustre  |16   | 1024 |SCR |?  |?  |? |
|HEAT  | instance spot |c6g.xlarge  | 4 |FSx Lustre  |16   | 2048 |SCR |?  |?  |? |

I'm thinking of using mainly execution time and cost to make comparative graphs. The throughput would be a plus.
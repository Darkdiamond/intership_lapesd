![Laboratory](../general_pictures/logo-lapesd-colorido-1024x298.png)

# Storage cloud

## Introduction

## What exist

* Amazon S3 (Simple Storage Service): Amazon S3 provides object storage for storing and retrieving any amount of data. It is highly scalable, durable, and designed for 99.999999999% (11 nines) of data durability. It is commonly used for backup and restore, data archiving, content distribution, and static website hosting.

* Amazon EBS (Elastic Block Store): Amazon EBS provides block-level storage volumes that can be attached to Amazon EC2 instances. It offers persistent storage for applications that require low-latency and high-performance block storage. EBS volumes can be used as primary storage for databases, file systems, and applications.

* Amazon EFS (Elastic File System): Amazon EFS offers a fully managed, scalable, and shared file storage service. It provides a simple interface to create and configure file systems, which can be accessed concurrently by multiple EC2 instances. EFS is ideal for content management systems, web serving, data sharing, and container storage.

* Amazon FSx: Amazon FSx provides fully managed file systems that are optimized for specific use cases. Currently, there are two offerings available: Amazon FSx for Windows File Server, which provides Windows-compatible file storage, and Amazon FSx for Lustre, which offers high-performance parallel file systems for workloads like HPC (high-performance computing), machine learning, and video processing.

* Amazon Glacier: Amazon Glacier is a secure, durable, and low-cost storage service for long-term data archiving and backup. It is optimized for infrequently accessed data and offers three different retrieval options with varying speeds and costs.

* Amazon S3 Glacier: Amazon S3 Glacier is a storage class within Amazon S3 that provides secure, durable, and low-cost archival storage. It is suitable for long-term data retention, compliance, and data archiving use cases. S3 Glacier offers configurable retrieval times for accessing archived data.

* Amazon S3 Glacier Deep Archive: Amazon S3 Glacier Deep Archive is the most cost-effective storage class within Amazon S3 for long-term data archival. It is designed for data that is accessed once or twice a year. Retrieval times for Deep Archive are longer compared to other storage classes, but it provides the lowest storage costs.

Please, note that two others FSx exist :\
Amazon FSx for OpenZFS is a fully managed file storage service that lets you launch, run, and scale fully managed file systems built on the open-source OpenZFS file system.\
Amazon FSx for NetApp ONTAP is a fully managed service that provides highly reliable, scalable, high-performing, and feature-rich file storage built on NetApp's popular ONTAP file system. FSx for ONTAP combines the familiar features, performance, capabilities, and API operations of NetApp file systems with the agility, scalability, and simplicity of a fully managed AWS service.

From that, the best option seems to be Amazon FSx for Lustre

For MPI (Message Passing Interface) applications, where high-performance computing and parallel processing are crucial, the Amazon FSx for Lustre storage service is often the best choice in AWS.

Amazon FSx for Lustre is a fully managed, high-performance file system that is optimized for workloads requiring fast data processing and parallel access. It provides sub-millisecond latencies and high throughput, making it ideal for MPI applications that heavily rely on parallel file I/O operations.

With Amazon FSx for Lustre, you can create a Lustre file system that can scale to hundreds of gigabytes per second of throughput, and millions of IOPS (Input/Output Operations Per Second). It supports the POSIX-compliant file system interface, which is commonly used by MPI applications.

Additionally, Amazon FSx for Lustre integrates well with other AWS services, such as Amazon EC2 instances and AWS Batch, allowing you to build a complete HPC (high-performance computing) environment. You can launch EC2 instances with the appropriate instance types and attach them to the Lustre file system to efficiently run your MPI workloads.

## The price (01/06/2023)

Amazon has a [pricing calculator](https://calculator.aws/#/) or you can search yourself [here](https://aws.amazon.com/fr/pricing/).

There is too much informations so I will just put links so you can see by yourself the up to date price, you can also change the regions to compare.

[Amazon S3 (Simple Storage Service)](https://aws.amazon.com/s3/pricing/?did=ap_card&trk=ap_card)

[Amazon EBS (Elastic Block Store)](https://aws.amazon.com/ebs/pricing/?did=ap_card&trk=ap_card)

[Amazon EFS (Elastic File System)](https://aws.amazon.com/efs/pricing/?did=ap_card&trk=ap_card)

[Amazon FSx](https://aws.amazon.com/fsx/pricing/?did=ap_card&trk=ap_card)

[Amazon Glacier](https://aws.amazon.com/s3/pricing/?did=ap_card&trk=ap_card)

[Amazon FSx for Lustre](https://aws.amazon.com/fsx/lustre/pricing/?refid=ap_card)

Bad news is that the price of Amazon FSx for lustre seems to be higher thant other and I'm not sure it's compatible with Amazon instance spot.

## To resume :

* Amazon S3 (Simple Storage Service):
    * Cost: Amazon S3 offers low-cost storage options, particularly for long-term data storage and archiving. However, costs can increase with data transfer and certain optional features.
    * Efficiency: S3 provides high durability, scalability, and availability. It is suitable for storing and retrieving large amounts of data, but the performance may be lower compared to other storage solutions.

* Amazon EBS (Elastic Block Store):
    * Cost: EBS pricing is based on the provisioned storage capacity and any provisioned IOPS. EBS volumes can be costlier compared to other storage solutions, especially when high-performance configurations are required.
    * Efficiency: EBS delivers low-latency, high-performance block storage suitable for databases, file systems, and applications. It offers consistent and predictable performance.

* Amazon EFS (Elastic File System):
    * Cost: EFS pricing is based on the storage used. While EFS is generally cost-effective for small to medium workloads, it can become expensive for high-capacity storage or high-throughput scenarios.
    * Efficiency: EFS provides scalable and shared file storage with high availability and performance. It allows multiple EC2 instances to access the file system concurrently.

* Amazon FSx:
    * Cost: The cost of FSx services depends on the provisioned storage capacity, data transfer, and optional features like Multi-AZ and backups. FSx can have higher costs compared to other storage solutions, especially for high-performance configurations.
    * Efficiency: FSx offers managed file systems optimized for specific use cases. FSx for Lustre provides high-performance parallel file systems, while FSx for Windows File Server delivers Windows-compatible file storage.

* Amazon Glacier:
    * Cost: Glacier is a cost-effective solution for long-term data archival. Storage costs are low, but additional costs may be associated with data retrieval and other operations.
    * Efficiency: Glacier is optimized for infrequent access and offers high durability. However, data retrieval times can be relatively slow, ranging from minutes to hours.

* Amazon S3 Glacier and Glacier Deep Archive:
    * Cost: Glacier and Glacier Deep Archive provide the lowest cost options for long-term data archival. However, retrieval times are longer compared to other storage solutions, and additional costs apply for data retrieval.
    * Efficiency: Glacier and Glacier Deep Archive are designed for infrequently accessed data with high durability. They are suitable for compliance, data archiving, and long-term retention.

## Thoses I would recommend :

* [Amazon FSx for Lustre](https://aws.amazon.com/fsx/lustre/pricing/?refid=ap_card) :
    * Cost: Amazon FSx for Lustre offers high-performance parallel file systems suitable for HPC (high-performance computing) workloads like MPI. However, it can be relatively more expensive compared to other storage solutions due to its performance capabilities.
    ![FSx_SSD](FSx_SSD.png)
    ![FSx_HDD](FSx_HDD.png)
    ![FSx_dataTransfert](FSx_dataTransfert.png)
    ![FSx_preFS](FSx_preFS.png)
    
    * Efficiency: FSx for Lustre provides low-latency, high-throughput storage that is well-suited for parallel file I/O operations required by MPI applications. It can deliver excellent performance for data-intensive and highly parallel workloads.
    \
    [Here what I found about performance](https://docs.aws.amazon.com/fsx/latest/LustreGuide/performance.html), I assume it'll be useful.
    ![SSD](FSx_SSD_perf.png)
    ![HDD](FSx_HDD_perf.png)

* [Amazon EFS (Elastic File System)](https://aws.amazon.com/efs/pricing/?did=ap_card&trk=ap_card) :
    * Cost: Amazon EFS provides a cost-effective option for storing and accessing data from multiple EC2 instances concurrently. It is generally more cost-effective compared to FSx for Lustre, especially for smaller to medium-sized workloads.
    ![EFS_ohio](EFS_ohio.png)
    ![EFS_princing](EFS_pricing.png)
    * Efficiency: While EFS may not match the performance levels of FSx for Lustre, it still offers decent performance and scalability for MPI applications. EFS is suitable when cost-efficiency is a priority, and the application's performance requirements are not extremely demanding.
    \
    [Here what I found about performance](https://docs.aws.amazon.com/efs/latest/ug/performance.html), I assume it'll be useful.
    ![Perf](EFS_perf.png)

The best region for price seems to be USA East(Ohio) and USA West(North Virginia) for EFS and USA East(Ohio), USA West(North Virginia), USA West(Oregon) and Asie-pacifique(Hyderabad).

## Other :
[Rescale](https://rescale.com/lp/hpc-search/?utm_campaign=emea-hpc-keywords-4-28&utm_medium=ppc&utm_source=google&utm_content=&utm_term=&utm_medium=ppc&utm_campaign=&utm_term=cloud%20hpc&utm_source=adwords&hsa_tgt=kwd-27960352242&hsa_net=adwords&hsa_cam=20054241031&hsa_acc=6247080214&hsa_ver=3&hsa_ad=656667371739&hsa_src=g&hsa_kw=cloud%20hpc&hsa_mt=e&hsa_grp=152262173207&gad=1&gclid=CjwKCAjwpuajBhBpEiwA_ZtfhQ77JZoIktRSlWfNpc2bm_wqr6x323smr6JRuOJcyVyXMa1mieOOVRoCV_kQAvD_BwE)

\
\
![Polytech](../general_pictures/Grenoble-INP-Polytech-Grenoble-couleur-RVB.png)
![Laboratory](../general_pictures/logo-lapesd-colorido-1024x298.png)

# FSx_Lustre 

## What I found :

Here you find different youtube video that are usefull:
* [Keeping Multiple S3 Buckets Synchronized with an FSx for Lustre File System](https://www.youtube.com/watch?v=LcTVHmBS7Us)

* [Amazon FSx for Lustre: Persistent Storage Overview](https://www.youtube.com/watch?v=ttCAqwnN_Qo) -> From what I understand. You create your file system (FSx Lustre) and you can access it simultaneously with multiples instances (EC2 in the video).

I think that this one is the most interesting, and because it's far too long, I've put links with time codes to the parts I think are relevant :
* [AWS re:Invent 2021 - Deep dive on Amazon FSx for Lustre | AWS Events](https://www.youtube.com/watch?v=m8_wy1xkwA8) -> It's transparent, it's automaticaly balance between differents severs.\
[Here explanation about transparency](https://youtu.be/m8_wy1xkwA8?t=369).\
[Here a problem with instance spot and how people manage to do it](https://youtu.be/m8_wy1xkwA8?t=2524).

## About terraform :

Here the link : https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/fsx_lustre_file_system

Here the sum up :
You should have something like this :
```
resource "aws_fsx_lustre_file_system" "example" {
  import_path      = "s3://${aws_s3_bucket.example.bucket}"
  storage_capacity = 1200
  subnet_ids       = [aws_subnet.example.id]
}
```
The only mandatory argument is the subnet_ids, the rest is optional. It's list of IDs for the subnets that the file system will be accessible from. File systems currently support only one subnet. The file server is also launched in that subnet's Availability Zone. From what I saw, it's already used in current terraform file so it's easy to find it.

Here some of the optional arguments : 

* The storage capacity  : It's what it seems to mean. It is optional, BUT if we don't creat the filesystem from a backup, then it is required.
* Backup_ids : Allow you to put the ID of a source backup to create the file system.
* Per_unit_storage_throughput : Allow you to choose the throughput, but there is a lot of conditions, I recommend to have a look at the official documentation.

these are thoses I thinks could be useful.

## How did I implement it in the tool ;

First I looked a the hcl_blueprints folder which is in HPCC_SERVICES. Here I took a copy of the aws in order to modify it and renaming it aws_FSxLustre. Since I knew we were using EBS, I use this to find where should I modify things. Then Vanderlei told me to have a look at the terraform website because itis where I can find the way to add FSx Lustre int the file. That is where I found the code structure (the one I'm speacking above). After that I tought it could work, but when I tried, an error saying that the provider is not defined occur. I think it refer to the cluster_config file, I missed something, like a file that list all the provider.

After a long time, I finaly understood. In the hpcc_api folder, there is a file named models.py which is simply the file setting out the various prerequisites, by which I mean the values accepted for certain fields and so on. 

It seems to work now. I will need to make more research to see if I can optimize the instalation by using optional arguments for FSx Lustre.

## About test

I was thinking of doing NAS Parallel Benchmarks (NPB), which is designed to help evaluate the performance of parallel supercomputers. But after discussing with it with Mr Castro, only [NPB-IS (from NAS)](https://www.nas.nasa.gov/software/npb.html) will remain. And I will use HEAT (from Vanderlei) because it already use the BLCR (Berkeley Labs Checkpoint-Restart) mechanism.

\
\
![Polytech](../general_pictures/Grenoble-INP-Polytech-Grenoble-couleur-RVB.png)